#########################################################
## Simulate Dynamics - 1 yr no control + 2 yrs control ##
#########################################################

## source("~/papers/inProgress/boosted/R/stochastic_BSIT_experiments.R")

###########################
## Load nimble functions ##
###########################
if (TRUE) ## FALSE 
    source("~/papers/inProgress/boosted/R/stochastic_BSIT.R")

#############################
## Choose a control method ##
#############################
CA <- commandArgs(TRUE)
if (length(CA)>0) {
    ## Used when on Migale 
    nimPrint("CA = ", CA)
    print(iSitMethod <- as.integer(CA)[1])
    print(qsubID     <- as.integer(CA)[2])
} else {
    ## Used when not on Migale
    iSitMethod   <- 2 # 1
    qsubID       <- 123456789
}
sitMethods <- c("SIT", "BSIT")
sitMethod  <- sitMethods[iSitMethod]
nimPrint("sitMethod = ", sitMethod)  

##########################################
## Set parameters for chosen SIT method ##
##########################################
(nSeqR <- 9)
if (sitMethod=="SIT") {    
    (seqR <- seq(0, 1600, l=nSeqR)) # (seqR <- seq(0, 1800, l=nSeqR))
    BSITmodel[["r"]] <- 0           # Use r=0 for no boosting, use r=Paras$r for BSIT 
} else if (sitMethod=="BSIT") {
    (seqR <- seq(0, 320, l=nSeqR)) # (seqR <- seq(0, 300, l=nSeqR))
    BSITmodel[["r"]] <- Paras$r    # Use r=0 for no boosting, use r=Paras$r for BSIT 
} else {
    stop("Choice not recognised.")
}

###############################################
## Set parameters for the four ADT scenarios ##
###############################################
(seqAlpha <- c(alphaCaputo, alphaCaputo, alphaAbad15, alphaAbad17))
(seqA     <- c(0, 10, 4, 2))
(alphaA <- seqAlpha * seqA)  ## Just for checking



## BSITmodel[["R"]]     <- seqR[1]
## BSITmodel[["A"]]     <- seqA[1]
## BSITmodel[["alpha"]] <- seqAlpha[1]
## BSITmodel[["time"]]  <- 0
approxDeltaT         <- 1E-5     # For discretising time for tau-leap type updates. Between 1E-3 & 1E-4 gives reasonable results. 1E-2 generates bias

#############################
## Initialise nimble model ##
#############################
## simulate(model=BSITmodel)
## simulate(model=BSITmodel, nodes=outputNodes)
## simulate(model=BSITmodel, nodes="propensity")
## nimCopy(from=BSITmodel, to=cBSITmodel, nodes=c(outputNodes,"R","A","r","alpha"))
## simulate(model=cBSITmodel)

#################
## Monte Carlo ##
#################
nRep <- 2 ## 10
nSim <- nRep*length(seqR)*length(seqA)
output <- vector("list", length(seqA))
nimPrint("nSim = ", nSim)
for (iA in 1:length(seqA)) { # iA=1
    idSim <- 0
    nSim4A <- nRep*length(seqR)
    output[[iA]] <- vector("list", nSim4A)
    for (iRep in 1:nRep) { # iRep = 1
        for (iR in 1:length(seqR)) { # iR=1
            idSim <- idSim + 1
            nimPrint("iRep=",iRep, " idSim=", idSim)
            ## Set key parameters & initial population
            (BSITmodel[["R"]]     <- seqR[iR])
            (BSITmodel[["A"]]     <- seqA[iA])
            (BSITmodel[["alpha"]] <- seqAlpha[iA])
            nimCopy(from=BSITmodel, to=cBSITmodel, nodes=c(outputNodes,"r","A","R","alpha"))
            simulate(model=cBSITmodel) ## Samples starting values (rpois) and updates propensities 
            ## Run simulation for 3 years 
            endOfPhase1 <- 3*365
            cBSITmodel[["time"]] <- 0
            cDirectMethod$run(Tmax=endOfPhase1, nimCopyDeltaT=1, printDeltaT=4*7, iPrintVars=c(9,1:8), verbose=FALSE, append=FALSE)
            ## 
            simBSIT <- as.matrix(cDirectMethod$mv)
            colnames(simBSIT) <- unlist(strsplit(colnames(simBSIT),split="\\[.."))
            simBSIT <- simBSIT[,outputNodes[c(iTime, 1:(iTime-1))]]
            ## Output
            R0 <- calculateR0(simBSIT[,"F"]+simBSIT[,"Fc"], dengueParas2) 
            output[[iA]][[idSim]]$sitMethod <- sitMethod
            output[[iA]][[idSim]]$alpha     <- BSITmodel[["alpha"]]
            output[[iA]][[idSim]]$A         <- BSITmodel[["A"]]
            output[[iA]][[idSim]]$R         <- BSITmodel[["R"]]
            output[[iA]][[idSim]]$simBSIT   <- cbind(simBSIT[,c("time","F","Fc","C")], R0)
        }
    }
}

####################################
## Save output to it's own folder ##
####################################
(outFileName <- paste0("monteCarlo_", sitMethod, "_", substring(date(), 12, 19), ".", round(runif(1,1,1E6)), ".Rdata"))
(folderName <- paste0("simulation_", sitMethod, "_", qsubID))
setwd(mainDir)
if (length(grep("simulations", dir()))==0)
    system("mkdir simulations")
setwd("simulations")
system(paste("mkdir", folderName))
setwd(folderName)
save(output, seqA, seqAlpha, file=outFileName)







## par(mar=c(4,5,1,4))
## plot(simBSIT[,"F"]+simBSIT[,"Fc"] ~ simBSIT[,"time"], typ="n", xlab="Time (days)",
##      ylim=c(0, 4000), 
##      ylab=TeX("$\\textit{F}_\\textit{Total}$"))
## for (ii in 1:length(output)) {
##     simBSIT <- output[[ii]]$simBSIT
##     if (ii==1) {
##         (ticksR0    <- pretty(c(0,simBSIT[,"R0"])))
##         ticksR0asF <- R0toF(ticksR0, dengueParas)
##         axis(4, at=ticksR0asF, labels=ticksR0)
##         mtext(TeX("$\\textit{R}_\\textit{0}$"), 4, line=2.5)
##         abline(h=R0toF(R0=1, dengueParas), col=rgb(1,0,0,0.5), lty=2, lwd=2)
##         abline(h=0, col="grey")
##         abline(v=365, col="green", lty=2, lwd=2)
##     }
##     lines(simBSIT[,"F"]+simBSIT[,"Fc"] ~ simBSIT[,"time"], lwd=1)
## }

## dengueParas$b     <- 0.25
## dengueParas$betaH <- 0.31
## dengueParas$betaF <- 0.31
## dengueParas$thetaF <- 1/8.23 ## 



## ###################################
## ## Effects of approxDeltaT - R=0 ##
## ###################################

## nimPrint(approxDeltaT, "  ", t1, "  ", mean(simBSIT[,"L"]), "  ", var(simBSIT[,"L"]))

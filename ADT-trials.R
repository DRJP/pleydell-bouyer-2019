## Code to support ADT-trials.org 
library(latex2exp) ## For Latex in graphics

####################################
## Caputo's enclosed garden study ##
####################################

## Duration
as.Date("11-08-2011", format="%d-%m-%Y") - as.Date("28-07-2011", format="%d-%m-%Y") ## 14
as.Date("15-09-2011", format="%d-%m-%Y") - as.Date("26-08-2011", format="%d-%m-%Y") ## 20

## Mortality (from Table 1)
(jmc1 <- 3/250)                 # Juvenile mortality control 1
(jmc2 <- 4/250)                 # Juvenile mortality control 2
(jme1 <- 125/250)               # Juvenile mortality experimental 1
(jme2 <- 131/250)               # Juvenile mortality experimental 2
(ei1  <- 1 - (1-jme1)/(1-jmc1)) # Emergence inhibition 1
(ei2  <- 1 - (1-jme2)/(1-jmc2)) # Emergence inhibition 2
(1-jmc1) * (1-ei1) - (1 - jme1) # Check. Should very be small (just computational error)
(1-jmc2) * (1-ei2) - (1 - jme2) # Check. Should very be small (just computational error)

######################
## Abad-Franch 2015 ##
######################
mb  <- 0.02                # albopictus mortality before ADT trial
md  <- 0.842               # Peak albopictus mortality during ADT trial - approximately month 3
(ei <- 1 - (1-md)/(1-mb))  # Emergence inhibition 63.3%
(1-mb) * (1-ei) - (1 - md) # Check. Should very be small (just computational error)
diff(c(as.Date("2011-01-12", format="%Y-%d-%m"), as.Date("2012-31-03", format="%Y-%d-%m")))*0.75 ## 91 days is 3/4 way through trial - when EI peaks.


##################
## Chandel 2016 ##
##################
mb  <- 0.02                # Based on fig 4 A & Abad-Franch 2015
md  <- 0.40                # Based on fig 5 A, weeks 2-4
(ei <- 1 - (1-md)/(1-mb))  # Emergence inhibition 63.3%
(1-mb) * (1-ei) - (1 - md) # Check

###############
## Unlu 2017 ##
###############
mb  <- 0.02                # Based on Abad-Franch 2015 & Chandel 2016
md  <- 0.70                # Reported mortality at 0.004 ppb
(ei <- 1 - (1-md)/(1-mb))  # Emergence inhibition 63.3%
(1-mb) * (1-ei) - (1 - md) # Check

######################
## Abad-Franch 2017 ##
######################
mb  <- 0.019               # From text, page 7
md  <- 0.797               # From text, page 7
(ei <- 1 - (1-md)/(1-mb))  # Emergence inhibition 63.3%
(1-mb) * (1-ei) - (1 - md) # Check

md  <- 0.89                # Value for May, from fig. 2
(ei <- 1 - (1-md)/(1-mb))  # Emergence inhibition 63.3%
(1-mb) * (1-ei) - (1 - md) # Check

diff(c(as.Date("2015-01-03", format="%Y-%d-%m"), as.Date("2015-31-05", format="%Y-%d-%m"))) ## 91 days to end of May


################################
## Setup for using BSIT model ##
################################
mainDir <- "~/papers/inProgress/boosted/R"
figDir  <- "/home/david/papers/inProgress/boosted/R/figures/2019_resubmission1/"
setwd(mainDir)
options(width=150)
library(sp)        ## Spatial data classes
library(fields)    ## For image.plot
library(NLRoot)    ## Finds roots
library(xtable)    ## Latex tables 
library(deSolve)   ## DE solver
library(rootSolve) ## Finds roots
library(latex2exp) ## For Latex in graphics
library(nimble)    ## For logit and nimPrint
## library(scam)   ## Shape constrained additive models - nice idea - but disapointing performance
source("boosted_functions.R")
useDellChism <- TRUE ## FALSE
source("boosted_parameters.R")
ParasOri <- Paras
setwd(figDir)


#######################################################################
## Calibration of alpha (ADT model) by fitting EI at a given time point
#######################################################################
print("PLOT 7 calib")
source("~/papers/inProgress/boosted/R/recalculate.R")
load(file="plot5_5.Rdata")

(calibData <- matrix(c(10.0, 20, 0.51, 0.20,  # Caputo
                       14.0, 91, 0.84, 0.45,  # Abad-Franch
                       20.0, 14, 0.39, 0.25,  # Chandel
                       19.8, 49, 0.69, 0.25,  # Unlu
                       1.54, 91, 0.89, 0.40), # Abad-Franch ## 1.54,152, 0.96, 0.40),# Abad-Franch (peak)
                     ncol=4, byrow = TRUE))
colnames(calibData) <- c("A",    # ADT station density / ha
                         "time", # Duration of treatment
                         "ei",   # Reported (or derived) emergence inhibition
                         "vol")  # Volume of water (litres) in sentinel traps 
study <- c("Caputo (2012)", "Abad-Franch (2015)", "Chandel (2016)", "Unlu (2017)", "Abad-Franch (2017)")

## Loop for finding optimal alpha, over discretised values, for each of the five studies
## SLOW - can disable if ranges in alphaMat are reasonable 
if (FALSE) { # TRUE
    ## For each study, loop over a range of potential values of alphas
    optAF  <- vector("list", length(study)) ## For each study, record vectors alpha and abs.error
    L <- 100
    alphaMat <- matrix(c(seq(0.0001, 0.01, l=L),
                         seq(0.005,   0.5, l=L),
                         seq(0.0001, 0.01, l=L),
                         seq(0.0001, 0.01, l=L),
                         seq(0.05,      5, l=L)), ncol=length(study))
    Cols  <- c("red",  "green","blue", "brown","orange")
    nStudies <- ncol(alphaMat)
    tSim  <- 365 
    for (jj in 1:nStudies) {
        optAF[[jj]] <- as.data.frame(matrix(0, nrow=L, ncol=2))
        names(optAF[[jj]]) <- c("alpha", "aber")
    }
    for (iii in 1:L) {
        print(iii)
        alphas  <- alphaMat[iii,] ##  c( 0.0035, 0.7,   0.002,   0.0024, 0.7) # Initial guess for alph in each study
        if (RECALC_RDATA) {
            fMax <- {}
            cMax <- {}
            ## source("../../boosted_functions.R")
            for (jj in 1:nStudies) { # jj = 1
                Paras       <- ParasOri
                Paras$A     <- calibData[jj,"A"] # Equivalent to 1 ADS every 31.6m.  Aim for EI~=60%. 10 ADS/ha & 60% ~= Caputo results.
                Paras$alpha <- alphas[jj]          # alpha = alphas[1]
                Paras$R     <- 0                 # 40
                eqK         <- eqCarryingCapacity(K1=K1, N=N, mE=mE, mL=mL, mP=mP, muE=muE, muP=muP, muF=muF, muM=muM, rho=rho, g=g, f=f)
                (State      <- eqK)       
                tCount      <- 0
                (output     <- c(tCount, State))
                nrowOutput  <- 1
                flatThresh  <- 1E-5
                almostFlat  <- FALSE
                ## nimble::nimPrint("alpha=", Paras$alpha)
                (MosquitoColsBSIT <- c("E","L","P","F","M","Fc"))
                (MosquitoColsBSIT <- which(is.element(names(State), MosquitoColsBSIT)))
                while (tCount <= tSim) {
                    (tCount     <- tCount + deltaT)    
                    (trajBSIT   <- ode(y = State, times = timesODE, parms = Paras, func = gradBoosted))
                    (State      <- tail(trajBSIT,1)[-1])
                    (output     <- rbind(output, c(tCount, State)))
                    (nrowOutput <- nrow(output))
                    sumAbsDiff  <- sum(abs(diff(tail(trajBSIT, n=2)[,-1])))
                    (almostFlat <- (sumAbsDiff < flatThresh))
                }
                trajBSIT     <- output 
                ## Emergence inhibition at larval sites (assumed to equal EI in ovitraps). 
                (EI <- 1 - (1+((trajBSIT[,"C"]/(Paras$V1*Paras$N))/Paras$EI50)^Paras$slope)^(-1))
                trajBSIT <- cbind(trajBSIT, EI)
                ## nimPrint(trajBSIT)
                totalPopBSIT <- rowSums(trajBSIT[,MosquitoColsBSIT+1])
                keep         <- totalPopBSIT >= 1
                keep         <- (keep==TRUE) | c(TRUE, keep)[1:length(keep)] ## So 1st element < 1 is also kept
                trajBSIT     <- trajBSIT[keep,]
                totalPopBSIT <- totalPopBSIT[keep]
                fMax <- c(fMax, max(trajBSIT[,"F"] + trajBSIT[,"Fc"]))
                cMax <- c(cMax, max(trajBSIT[,"C"])) 
                ## save(Paras, trajBSIT, totalPopBSIT, file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
                ##
                (optAF[[jj]][iii,"alpha"] <- alphas[jj])
                (optAF[[jj]][iii,"aber"]  <- abs(trajBSIT[1+calibData[jj,"time"],"EI"] - calibData[jj,"ei"]))            
            }
        }
    }
}

## Visual check on optimisation
pdf(file="ADT-PLOT_minimise-abserr.pdf", width=6, height=12)
par(mfrow=c(5,1), cex.axis=1.5, cex.lab=1.5, cex.main=1.5, mar=c(4.5,5,2,1))
alphasOpt <- rep(0, nStudies)
for (jj in 1:nStudies) {
    iMin <- which(optAF[[jj]][,"aber"] == min(optAF[[jj]][,"aber"]))
    alphasOpt[jj] <- optAF[[jj]][iMin,"alpha"]
    plot(optAF[[jj]][,"aber"] ~ optAF[[jj]][,"alpha"], ylab="Abs. Error", xlab=TeX("$\\alpha$"))
    title(TeX(paste0(study[jj], ": $\\hat{\\alpha} = $", alphasOpt[jj])))
    abline(v=optAF[[jj]][iMin,"alpha"], col=rgb(0,0,1,0.4))
}
dev.off()

## Save output 
(ACaputo     <- calibData[1,"A"])
(alphaCaputo <- alphasOpt[1])
(AAbad       <- calibData[5,"A"])
(alphaAbad   <- alphasOpt[5])
save(calibData, alphasOpt, study, nDaysInYear, file="alphasOpt.Rdata")
load(file="alphasOpt.Rdata")



##########################################
## Re-run simulations at optimal alphas ##
##########################################
if (RECALC_RDATA) {
    load(file="plot5_5.Rdata")
    load(file="alphasOpt.Rdata")
    alphas <- alphasOpt
    fMax <- {}
    cMax <- {}
    ## source("../../boosted_functions.R")
    for (jj in 1:nStudies) { # jj = 1
        Paras       <- ParasOri
        Paras$A     <- calibData[jj,"A"]   # Equivalent to 1 ADS every 31.6m.  Aim for EI~=60%. 10 ADS/ha & 60% ~= Caputo results.
        Paras$alpha <- alphas[jj]          # alpha = alphas[1]
        Paras$R     <- 0                   # 40
        eqK         <- eqCarryingCapacity(K1=K1, N=N, mE=mE, mL=mL, mP=mP, muE=muE, muP=muP, muF=muF, muM=muM, rho=rho, g=g, f=f)
        (State      <- eqK)       
        tCount      <- 0
        (output     <- c(tCount, State))
        nrowOutput  <- 1
        flatThresh  <- 1E-5
        almostFlat  <- FALSE
        ## nimble::nimPrint("alpha=", Paras$alpha)
        (MosquitoColsBSIT <- c("E","L","P","F","M","Fc"))
        (MosquitoColsBSIT <- which(is.element(names(State), MosquitoColsBSIT)))
        while (tCount <= tSim) {
            (tCount     <- tCount + deltaT)    
            (trajBSIT   <- ode(y = State, times = timesODE, parms = Paras, func = gradBoosted))
            (State      <- tail(trajBSIT,1)[-1])
            (output     <- rbind(output, c(tCount, State)))
            (nrowOutput <- nrow(output))
            sumAbsDiff  <- sum(abs(diff(tail(trajBSIT, n=2)[,-1])))
            (almostFlat <- (sumAbsDiff < flatThresh))
        }
        trajBSIT     <- output 
        ## Emergence inhibition
        (EI <- 1 - (1+((trajBSIT[,"C"]/(Paras$V1*Paras$N))/Paras$EI50)^Paras$slope)^(-1))
        trajBSIT <- cbind(trajBSIT, EI)
        ## nimPrint(trajBSIT)
        totalPopBSIT <- rowSums(trajBSIT[,MosquitoColsBSIT+1])
        keep         <- totalPopBSIT >= 1
        keep         <- (keep==TRUE) | c(TRUE, keep)[1:length(keep)] ## So 1st element < 1 is also kept
        trajBSIT     <- trajBSIT[keep,]
        totalPopBSIT <- totalPopBSIT[keep]
        fMax <- c(fMax, max(trajBSIT[,"F"] + trajBSIT[,"Fc"]))
        cMax <- c(cMax, max(trajBSIT[,"C"])) 
        save(Paras, trajBSIT, totalPopBSIT, file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
    }
}

save(cMax, fMax, Cols, calibData, file="plotParams.Rdata")

## ############################################
## ## Combine with plot-3c (equilibria, ADT) ##
## ############################################
## if (TRUE) { ## FALSE
##     ## PLOT PARAS
##     H         = 1700
##     CexAnot   = 2
##     CexAxis   = 1.6 ## 1.7
##     CexLab    = 2
##     CexLeg    = 2.2
##     CexLegSml = 2
##     CexLegTex = 1.8
##     CexMain   = 2.3
##     CexMtxt   = 1.8 # 2.1
##     CexPnt    = 2
##     OMA       = c(0,0,0,0)
##     MAR       = c(4.5,6,3,1.2)
##     PADJ      = 0.5
##     ##
##     figDir  <- "/home/david/papers/inProgress/boosted/R/figures/2019_resubmission1"   
##     filename <- "plot-3c_and_7calib.tiff"
##     TIFF <- TRUE ## FALSE
##     if (TIFF) {
##         setwd(figDir)
##         tiff(filename=filename, compression="lzw", res=300, width=2*H, height=2*H)
##     }
##     par(mfrow=c(2,2), cex.axis=CexAxis, cex.lab=CexLab, cex.main=CexMain, oma=OMA, mar=MAR)
##     ## ###############
##     ## Plot-7calib ##
##     ## ###############
##     load("plot7calib_ADT_Abad-Franch (2015).Rdata")
##         for (jj in 1:nrow(calibData)) {
##         load(file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
##         if (jj == 1) {
##             plot (trajBSIT[,"EI"]~trajBSIT[,1], typ="l", lwd=3,
##                   xlab="", ylab="", xlim=c(0, tSim), ylim=c(0,1), col=Cols[jj])
##             mtext(TeX("Emergence Inhibition ($\\textit{EI}$)"), 2, cex=CexMtxt, line=3)
##             mtext(TeX("Time in days"), 1, cex=CexMtxt, line=3.2)
##             abline(h=0)
##             mtext(paste0("R=",Paras$R),3, outer=TRUE, cex=CexMtxt)
##         } else {
##             lines(trajBSIT[,"EI"]~trajBSIT[,1], lwd=3, col=Cols[jj])
##         }
##     }
##     dataPCH <- 4
##     for (jj in 1:nrow(calibData)) {        
##         points(calibData[jj,"ei"] ~ calibData[jj,"time"], pch=dataPCH, cex=3, col=Cols[jj])    
##     }
##     legend(150, 0.35, bty="n", legend=study, col=Cols, lwd=3, pch=dataPCH, cex=1.5)
##     for (jj in 1:nrow(calibData)) {
##         load(file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
##         if (jj == 1) {
##             plot (trajBSIT[,"C"]~trajBSIT[,1], typ="l", lwd=3,
##                   xlab="", ylab="", col=Cols[jj], xlim=c(0, tSim), ylim=c(0,max(cMax)))
##             mtext(TeX("PPF at larval sites (\\textit{C})"), 2, cex=CexMtxt, line=3)
##             mtext(TeX("Time in days"), 1, cex=CexMtxt, line=3.2)
##             abline(h=0)
##         } else {
##             lines(trajBSIT[,"C"]~trajBSIT[,1], lwd=3, col=Cols[jj])
##         }
##     }
##     for (jj in 1:nrow(calibData)) {
##         load(file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
##         if (jj == 1) { # jj = 1
##             plot (trajBSIT[,"F"]+trajBSIT[,"Fc"]~trajBSIT[,1], typ="l", lwd=3,
##                   xlab="", ylab="", col=Cols[jj], xlim=c(0, tSim), ylim=c(0,max(fMax)))
##             mtext(TeX("Females (\\textit{F}+\\textit{F_c})"), 2, cex=CexMtxt, line=3)
##             mtext(TeX("Time in days"), 1, cex=CexMtxt, line=3.2)
##             abline(h=0)
##         } else {
##             lines(trajBSIT[,"F"]+trajBSIT[,"Fc"]~trajBSIT[,1], col=Cols[jj],  lwd=3)
##         }
##     }
##     ## ##########
##     ## PLOT-3c ##
##     ## ##########
##     load(file="plot3c.Rdata")
##     source("../../boosted_functions.R")
##     Paras <- ParasOri
##     (rmax <- max(ELR0$R, A, na.rm=TRUE))
##     (maxA <- 400)
##     plot(output[,"M2"] ~ output[,"A"], typ="n",  
##          lwd=3, ## las=1,
##          cex.axis=CexAxis, cex.lab=CexLab,
##          xlim=range(pretty(c(0, maxA))),
##          ylim=c(0, max(output[,"M2"])),
##          xlab="", ylab="")
##     ## A <- 2000
##     abline(v=0, col="darkgrey")
##     abline(h=0, col="darkgrey")
##     abline(h=eqK[["M"]], col="lightgreen", lwd=2)
##     lines(output[,"A"], output[,"M1"], lwd=3, lty=3)
##     lines(output[,"A"], output[,"M2"], lwd=3)
##     mtext(TeX("Nb. dissemination stations (\\textit{A})"), 1, cex=CexMtxt, line=3.2) ## bquote('Nb. dissemination stations  ('*italic(A)*')'),
##     mtext(TeX("Wild males at equillibrium ($\\textit{M}^*$)"), 2, cex=CexMtxt, line=3)
##     if (FALSE) { ## TRUE
##         xy <- as.list(c(y=1500, x=250))
##         text(xy$x, xy$y, "M grows", cex=CexAnot) ## cex=2)
##         xy <- as.list(c(x=1125, y=3000))
##         text(xy$x, xy$y, "M declines", cex=CexAnot) ## cex=2)
##     } else {
##         print("Toggle flag to add annotation")
##     }
## }
## if (TIFF) {
##     dev.off()
##     ## system(paste("convert",  FILENAME, sub("tiff", "png", FILENAME)))
## }


####################################################
## Alternatively, also with plot-17 (RThresh ~ A) ##
####################################################
tSim <- 365
if (TRUE) { ## FALSE
    ## PLOT PARAS
    H         = 1700
    CexAnot   = 2
    CexAxis   = 1.6 ## 1.7
    CexLab    = 2
    CEXLEG    = 1.5## 2.2
    CEXLEGSml = 2
    CEXLEGTex = 1.8
    CexMain   = 2.3
    CEXMTXT   = 1.7 # 2.1
    CexPnt    = 2
    OMA       = c(0,0,0,0)
    MAR       = c(4, 5, 4.5, 1.2)
    PADJ      = 0.5
    LWD       = 3 # for plots
    LWDab     = 2 # for abline
    colab     = "darkgrey"
    ##
    figDir  <- "/home/david/papers/inProgress/boosted/R/figures/2019_resubmission1"   
    ## filename <- "plot-3c_and_7calib.tiff"
    filename <- "FIGURE-3.tiff"
    load("alphasOpt.Rdata")
    load("plotParams.Rdata")
    TIFF <- TRUE ## FALSE
    if (TIFF) {
        setwd(figDir)
        tiff(filename=filename, compression="lzw", res=300, width=2*H, height=2*H)
    }
    par(mfrow=c(2,2), cex.axis=CexAxis, cex.lab=CexLab, cex.main=CexMain, oma=OMA, mar=MAR)
    ## ###############
    ## Plot-7calib ##
    ## ###############
    load("plot7calib_ADT_Abad-Franch (2015).Rdata")
        for (jj in 1:nrow(calibData)) {
        load(file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
        if (jj == 1) {
            plot (trajBSIT[,"EI"]~trajBSIT[,1], typ="l", lwd=LWD,
                  xlab="", ylab="", xlim=c(0, tSim), ylim=c(0,1), col=Cols[jj])
            mtext(TeX("Emergence Inhibition ($\\textit{EI}$)"), 2, cex=CEXMTXT, line=3)
            mtext(TeX("Time (days)"), 1, cex=CEXMTXT, line=3.2)
            abline(h=0, lwd=LWDab, col=colab)
            mtext(paste0("R=",Paras$R),3, outer=TRUE, cex=CEXMTXT)
        } else {
            lines(trajBSIT[,"EI"]~trajBSIT[,1], lwd=LWD, col=Cols[jj])
        }
    }
    dataPCH <- 4
    for (jj in 1:nrow(calibData)) {        
        points(calibData[jj,"ei"] ~ calibData[jj,"time"], pch=dataPCH, cex=3, col=Cols[jj])    
    }
    legend(150, 0.35, bty="n", legend=study, col=Cols, lwd=LWD, pch=dataPCH, cex=1.5)
    ## 
    for (jj in 1:nrow(calibData)) {
        load(file=paste0("plot7calib_ADT_", study[jj], ".Rdata"))
        if (jj == 1) { # jj = 1
            plot (trajBSIT[,"F"]+trajBSIT[,"Fc"]~trajBSIT[,1], typ="l", lwd=LWD,
                  xlab="", ylab="", col=Cols[jj], xlim=c(0, tSim), ylim=c(0,max(fMax)))
            mtext(TeX("Females (\\textit{F}+\\textit{F_c})"), 2, cex=CEXMTXT, line=3)
            mtext(TeX("Time (days)"), 1, cex=CEXMTXT, line=3.2)
            abline(h=0, col=colab)
        } else {
            lines(trajBSIT[,"F"]+trajBSIT[,"Fc"]~trajBSIT[,1], col=Cols[jj],  lwd=LWD)
        }
    }
    ## ##########
    ## PLOT-3c ##
    ## ##########
    load(file="plot3c.Rdata")
    source("../../boosted_functions.R")
    Paras <- ParasOri
    (rmax <- max(ELR0$R, A, na.rm=TRUE))
    (maxA <- 400)
    plot(output[,"M2"] ~ output[,"A"], typ="n",  
         cex.axis=CexAxis, cex.lab=CexLab,
         xlim=range(pretty(c(0, maxA))),
         ylim=c(0, max(output[,"M2"])),
         xlab="", ylab="")
    ## A <- 2000
    abline(v=0, col=colab) # , col="darkgrey")
    abline(h=0, col=colab) # , col="darkgrey")
    abline(h=eqK[["M"]], col="lightgreen", lwd=LWDab)
    lines(output[,"A"], output[,"M1"], lwd=LWD, lty=3)
    lines(output[,"A"], output[,"M2"], lwd=LWD)
    mtext(TeX("Nb. dissemination stations (\\textit{A})"), 1, cex=CEXMTXT, line=3.2) ## bquote('Nb. dissemination stations  ('*italic(A)*')'),
    mtext(TeX("Wild males at equillibrium ($\\textit{M}^*$)"), 2, cex=CEXMTXT, line=3)
    if (FALSE) { ## TRUE
        xy <- as.list(c(y=1500, x=250))
        text(xy$x, xy$y, "M grows", cex=CexAnot) ## cex=2)
        xy <- as.list(c(x=1125, y=3000))
        text(xy$x, xy$y, "M declines", cex=CexAnot) ## cex=2)
    } else {
        print("Toggle flag to add annotation")
    }
    #############
    ## Plot-17 ##
    #############
    load(file="plot17.Rdata")
    load(file="plot5_5.Rdata")
    load(file="alphasOpt.Rdata")
    alphas <- alphasOpt
    ## 
    YLIM <- c(0, max(na.rm=TRUE, pretty(output[,"RThreshSIT"])))
    XLIM <- c(0, 4300)
    plot(output[,"RThreshSIT"] ~ output[,"A"], typ="n",
         ylim=YLIM, xlim=XLIM, xlab="", ylab="", axes=FALSE)
    abline(v=0, lwd=LWDab, col=colab)
    abline(h=0, lwd=LWDab, col=colab)
    lines(output[,"RThreshSIT"] ~ output[,"A"], col="blue", lwd=LWD)
    lines(output[,"RThreshBSIT"] ~ output[,"A"], col="red", lwd=LWD)
    iMatch <- which(abs(output[,"RThreshSIT"]-output[1,"RThreshBSIT"])==min(abs(output[,"RThreshSIT"]-output[1,"RThreshBSIT"])))
    abline(v=output[iMatch,"A"], lwd=LWDab, lty=2, col=rgb(1,0,0,0.5))
    mtext(TeX("Dissemination stations ($\\textit{A}_C$)"), 1, line=3, cex=CEXMTXT)
    mtext(TeX("Dissemination stations ($\\textit{A}_{AF}$)"), 3, line=1.7, cex=CEXMTXT)
    mtext(TeX("$\\textit{R}_{Thresh}$"), 2, line=2.2, cex=CEXMTXT)
    (seqACaputo <- seq(0,4000,by=1000))
    (seqAAbad <- seqACaputo * alphaCaputo / alphaAbad)
    (seqAAbad <- unique(round(pretty(seqAAbad))))
    (seqACaputo <- seqAAbad * alphaAbad / alphaCaputo)
    par("mgp")[2]
    axis(3,at=seqACaputo, lab=rep("", length(seqAAbad) ))
    axis(3,at=seqACaputo, lab=seqAAbad, line = -0.3, tick=FALSE)
    axis(2)
    axis(1)
    legend("top", horiz = TRUE, col=c("blue","red"),legend=c("SIT","BSIT"),lwd=LWD, cex=CEXLEG, box.col="white")
    box(lwd=1)   
}
if (TIFF) {
    dev.off()
    ## system(paste("convert",  FILENAME, sub("tiff", "png", FILENAME)))
}

head(output$RThreshBSIT,1) # 281.6795
tail(output$RThreshBSIT,1) # 273.7831

## Density estimates from Dutra et al PLOS NTD 2015
K  <- c(1084, 8499, 2993, 9705, 2518)
ha <- c(2.755, 3.515, 1.461, 19.185, 13.695)
H  <- c(2992, 1196, 2425, 1423, 3212)
site <- c("amorim","jurujuba","tubiacanga","villa-valqueire","urca")

## Human density per ha
H/ha ## 1086.02541  340.25605 1659.82204   74.17253  234.53815

## Mosquito density per ha
K/ha ## 393.4664 2417.9232 2048.5969  505.8640  183.8627

## Vector/host ratio
K/H ## 0.3622995 7.1061873 1.2342268 6.8200984 0.7839352



## Biting density from Carrieri et al (J. Med Ent) 2012
## From table 3
mean <- c(0.73, 1.00, 0.60, 0.53, (1.27+0.96)/2, 3.8, 1.93, 2.2, 5.73, 2.73)  ## The fifth observation appears under dispersed - resulting in a negative p for the negative binomial.
stdv <- c(1.33, 1.25, 0.83, 1.06, (1.27+0.96)/2, 3.93, 2.12, 2.51, 4.48, 2.4) ## Since this might arise from sampling error, we average the parameters and assume a poisson distribution.
(p   <- 1 - mean/(stdv^2)) ## This is p for wikipedia = 1-p for R
(r   <- mean*(1-p)/p)
p
r
ii <- 1
Cols <- rainbow(length(mean))
To <- 20
N <- 1E5
curve(dnbinom(x, size=r[ii], prob=1-p[ii]), from=0, to=To, col=Cols[1], n=To+1, ylab="Probability for integer x", main="Neg. binomial (interpolated) repressentation of Carrieri HLC data")
simX <- rnbinom(n=N, size=r[ii], prob=1-p[ii])
simM <- mean(simX)
simS <- sd(simX)
q95 <- qnbinom(p=0.95,  size=r[ii], prob=1-p[ii])
for (ii in 2:length(mean)) {
    curve(dnbinom(x, size=r[ii], prob=1-p[ii]), from=0, to=To, add=TRUE, col=Cols[ii], n=To+1)
    simX <- rnbinom(n=N, size=r[ii], prob=1-p[ii])
    simM <- c(simM, mean(simX))
    simS <- c(simS, sd(simX))
    q95 <- c(q95, qnbinom(p=0.95,  size=r[ii], prob=1-p[ii]))
}
## Check for correct parameterisation using reproduced mean and stdv
cbind(mean, simM)
cbind(stdv, simS)
q95  ## 3  3  2  3  3 12  6  7 14  7

mean(mean) ## 2.0365

## Bite rate in Carrieri
GC <- 4    ## duration of gonotrophic cycle in summer
h  <- 0.86 ## Proportion of bites on humans
mf <- 1.2  ## 20% of females do multiple feeding
1/GC*h*mf  ## 0.258


5.73/154 * 100^2         ##  372 ## Extrapolation of bite rate over a 7m radius area to 1ha.
5.73/154 * 100^2 / 0.5   ##  744 ## Divide by daily bite rate to get female density over 1ha. Assuming no profylaxis.
5.73/154 * 100^2 / 0.258 ## 1442 ## Divide by daily bite rate to get female density over 1ha. Assuming Carrieri's parameters above

14/154 * 100^2 / 0.258   ## 3523 ## 5% of sites in Forli expected to have this density of females in August based on negative binomial.


5.73/(pi*7^2) * 100^2 / 0.258 ## 1442.7 females per ha
14  /(pi*7^2) * 100^2 / 0.258 ## 3525.0 females per ha

## Carrieri et al 2004, table 4
vub <- c(0.1, 0.5, 1, 5, 10, 50, Inf)          # Upper bound on volume category
naa <- c(46, 173, 102, 99, 35, 33, 47)         # Nb. Ae. albopictus larval sites
maa <- c(13.9,28.4,38.1,70.1,276.8,177.8,3020) # Mean Ae. albopictus larval / site
naa*maa
ncp <- c(7,68,75,154,52,167,256)
mcp <- c(57.2,50.7,72.9,144,480,1113,12306.6)
tab4 <- as.data.frame(cbind(Ae.albo=naa*maa, C.pip=ncp*mcp))
cbind(100*tab4[,"Ae.albo"]/sum(tab4[,"Ae.albo"]), 100*tab4[,"C.pip"]/sum(tab4[,"C.pip"]))

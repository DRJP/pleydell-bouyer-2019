#!/bin/sh

## The following takes the qsub job number and prints it to the qsub output file in ~/
## Note. This works in BASH shells but not C shells.
## By default migale uses a C shell
## So use a -S /bin/sh argument when using qsub

iControlMethod=$1
qsubID=$JOB_ID
echo "qsub process number is" $qsubID

## Now call the R script
echo "Now source the R script..."
echo 'source("/projet/extern/work/dpleydell/papers/inProgress/boosted/R/stochastic_BSIT_experiments.R");' | R --vanilla --quiet --args $iControlMethod $qsubID
